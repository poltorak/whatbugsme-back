const Router = require('express').Router;
const router = new Router();

const user  = require('./model/user/user-router');
const bug  = require('./model/bug/bug-router');
const invitation  = require('./model/invitation/invitation-router');


router.route('/').get((req, res) => {
  res.json({ message: 'Welcome to whatbugsme-backend API!' });
});

router.use('/user', user);
router.use('/bug', bug);
router.use('/invitation', invitation);


module.exports = router;
