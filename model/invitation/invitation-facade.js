const Model = require('../../lib/facade');
const invitationSchema  = require('./invitation-schema');

class InvitationModel extends Model {}

module.exports = new InvitationModel(invitationSchema);
