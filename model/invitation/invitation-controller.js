const Controller = require('../../lib/controller');
const invitationFacade  = require('./invitation-facade');

class InvitationController extends Controller {}

module.exports = new InvitationController(invitationFacade);
