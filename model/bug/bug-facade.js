const Model = require('../../lib/facade');
const bugSchema  = require('./bug-schema');

class BugModel extends Model {}

module.exports = new BugModel(bugSchema);
