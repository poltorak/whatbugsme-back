const Controller = require('../../lib/controller');
const bugFacade  = require('./bug-facade');
const _ = require('lodash');
const moment = require('moment');

function rand(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

class BugController extends Controller {
  getMock(req, res) {
    const itemRange = _.range(1, 6);
    const bugs = [];
    itemRange.forEach((item) => {
      bugs.push({
        _id: _.uniqueId(),
        content: 'Lorem ipsum dolor et.',
        rating: rand(-20, 20),
        addedAt: moment(`2017-01-${rand(10, 30)}`)
      });
    });
    return res.status(200).json(bugs);
  }
}


module.exports = new BugController(bugFacade);
