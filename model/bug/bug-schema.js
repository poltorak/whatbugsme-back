const user = require('../user/user-schema');
const mongoose = require('mongoose');
const Schema   = mongoose.Schema;


const bugSchema = new Schema({
  content: { type: String, required: true },
  rating: { type: Number },
  addedAt: { type: Date },
  addedBy: { type: mongoose.Schema.Types.ObjectId, ref: user }
});


module.exports = mongoose.model('Bug', bugSchema);
